extends KinematicBody2D

var speed = 200
var killing = 0
var human
var hostile = 0
var animation = "idleRight"
var vision = "right"

var velocity = Vector2.ZERO

func play(animation):
	$AnimatedSprite.play(animation)

func playDead():
	animation = "dead"
	play(animation)
	
func playKill():
	$AudioStreamPlayer2D.play()
	animation = "kill"
	play(animation)
	
func playIdleRight():
	animation = "idleRight"
	play(animation)
	
func killHuman():
	killing = 1
	human.set_physics_process(false)
	playKill()
	yield(get_tree().create_timer(1),"timeout")
	human.playDie()
	yield(get_tree().create_timer(0.4),"timeout")
	human.queue_free()
	playIdleRight()
	global.souls += 1
	killing = 0
	
func _ready():
	$AnimatedSprite.play("idleRight")

func get_input():
	if $AnimatedSprite.animation != animation:
		$AnimatedSprite.play(animation)
	
	velocity = Vector2.ZERO
	if Input.is_action_pressed('ui_right') && killing == 0:
		vision = "right"
		velocity.x += 1
		if hostile == 0:
			animation = "walkRight"
		else:
			animation = "hostileRight"
	if Input.is_action_pressed('ui_left') && killing == 0:
		vision = "left"
		velocity.x -= 1
		if hostile == 0:
			animation = "walkLeft"
		else:
			animation = "hostileLeft"
	if Input.is_action_pressed('ui_down') && killing == 0:
		velocity.y += 1
	if Input.is_action_pressed('ui_up') && killing == 0:
		velocity.y -= 1
	if Input.is_action_just_pressed("ui_select") && killing == 0:
		if human:
			killHuman()
	
	# Make sure diagonal movement isn't faster
	velocity = velocity.normalized() * speed
	

func _physics_process(delta):
	get_input()
	velocity = move_and_slide(velocity)


func _on_Area2D2_body_entered(body):
	if body.name == "Human":
		if vision == "right":
			animation = "hostileRight"
		else:
			animation = "hostileLeft"
		play(animation)
		hostile = 1
		human = body
		human.playInstaKill()

func _on_Area2D2_body_exited(body):
	if body.name == "Human":
		if vision == "right":
			animation = "idleRight"
		else:
			animation = "idleLeft"
		play(animation)
		hostile = 0
		human.playDefault()
		human = null
