extends Node

export(String) var scene_to_load

func _on_Area2D_body_entered(body):
	if global.souls == 5:
		get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
	else:
		global.notif = "Not enough souls. Collect more!"

func _on_Area2D_body_exited(body):
	global.notif = ""
