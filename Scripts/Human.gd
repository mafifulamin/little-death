extends KinematicBody2D

var speed = 70  # speed in pixels/sec
var state = "patrol"
var direction = 1
var playerInLight

export (String, "loop", "linier") var patrolType = "linier"
onready var pathFollow = get_parent()

func playDefault():
	$AnimatedSprite.play("default")

func playInstaKill():
	$AnimatedSprite.play("instakill")
	
func playDie():
	$AnimatedSprite.play("die")

func _process(delta):
	if playerInLight:
		var space_state = get_world_2d().direct_space_state
		var result = space_state.intersect_ray(global_position, playerInLight.position, [self], collision_mask)
		if result:
			if result.collider == playerInLight:
				playerInLight.playDead()
				yield(get_tree().create_timer(0.5),"timeout")
				get_tree().change_scene(str("res://Scenes/GameOver.tscn"))
	
func patrol(delta):
	if patrolType == "loop":
		pathFollow.offset += speed * delta
	else:
		if direction == 1:
			if pathFollow.unit_offset > 0.99:
				yield(get_tree().create_timer(0.3),"timeout")
				rotation_degrees = lerp(rotation_degrees, 180,0.1)
				yield(get_tree().create_timer(1),"timeout")
				direction = 0
			else:
				pathFollow.offset += speed * delta
		else:
			if pathFollow.unit_offset < 0.01:
				yield(get_tree().create_timer(0.3),"timeout")
				rotation_degrees = lerp(rotation_degrees, 0,0.1)
				yield(get_tree().create_timer(1),"timeout")
				direction = 1
			else:
				pathFollow.offset -= speed * delta

func _physics_process(delta):
	if state == "patrol":
		patrol(delta)

func _on_Area2D_body_entered(body):
	if body.name == "Reaper":
		playerInLight = body


func _on_Area2D_body_exited(body):
	if body.name == "Reaper":
		playerInLight = null
