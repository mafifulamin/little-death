extends LinkButton

export(String) var scene_to_load

func _on_LinkButton_pressed():
	global.souls = 0
	global.notif = ""
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
