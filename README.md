# Little Death

[<img src="https://static.itch.io/images/badge.svg" width="200">](https://mafifulamin.itch.io/little-death)

## Story

You are The God of Death. Due to unknown circumstances, you find yourself trapped somewhere with almost all of your death energy gone, makes you powerless. Search the area for humans and kill them to collect their souls and gain your lost energy.

## Objective

Collect enough souls to activate the magic circle.

## Diversifier Implementations

Tsunomaki Janken: Your weakness is light that kills you when you touch it. On the other hand, humans are afraid of the dark so you can catch them off guard from the shadow.

Phasmophobia: The game uses a really few of light sources that makes it hard to navigate through the map. Pay attention to patroling humans and their flashlights.

Berserk: If you are caught by the humans with their flashlights you will instantly die.

## Keybinds

Arrow keys: Walk

E: Kill Human

## Assets

Visual

https://samuellee.itch.io/reaper-animated-pixel-art

https://darkpixel-kronovi.itch.io/undead-executioner

https://assetbakery.itch.io/horror-house-tileset

Sounds

https://mixkit.co/license/

https://spicyoverlord.itch.io/scary-ambient-sounds

Font

https://ashurcollective.itch.io/too-much-ink
